const express = require('express');
const conexion = require(__dirname + '/../database/conexion');

let router = express.Router();

router.get('/', (req, res) => {
    conexion.query("SELECT * FROM autor",
        (error, resultado, campos) => {
            if (error) {
                res.status(500).send({
                    ok: false,
                    mensaje: "Error listando autores"
                });
            }
            else {
                res.status(200).send({
                    ok: true,
                    mensaje: "Se han obtenido los autores correctamente",
                    data: resultado
                });
            }
        });
});

module.exports = router;