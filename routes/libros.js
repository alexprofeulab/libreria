const express = require('express');
const conexion = require(__dirname + '/../database/conexion');
const utilImages = require(__dirname + '/../utils/util-images');
const config = require(__dirname + '/../config/config');

let router = express.Router();

router.get('/', (req, res) => {
    conexion.query("SELECT * FROM libros",
        (error, resultado, campos) => {
            if (error) {
                res.status(500).send({
                    ok: false,
                    mensaje: "Error listando libros"
                });
            }
            else {
                res.status(200).send({
                    ok: true,
                    mensaje: "Se han obtenido los libros correctamente",
                    data: resultado
                });
            }
        });
});

router.get('/:id', (req, res) => {
    conexion.query("SELECT * FROM libros WHERE COD=?", req.params.id,
        (error, resultado, campos) => {
            if (error) {
                res.status(500).send({
                    ok: false,
                    mensaje: "Error obteniendo el libro"
                });
            }
            else {
                if (resultado.length === 0) {
                    res.status(404).send({
                        ok: false,
                        mensaje: "No existe ningún libro con id " + req.params.id,
                    });
                }
                else {
                    res.status(200).send({
                        ok: true,
                        mensaje: "Se ha obtenido el libro " + req.params.id + " correctamente",
                        data: resultado[0]
                    });
                }
            }
        });
});

router.delete('/:id', (req, res) => {
    conexion.query("DELETE FROM libros WHERE COD = ?",
        req.params.id, (error, resultado, campos) => {
            if (error) {
                res.status(500)
                    .send({
                        ok: false,
                        error: "Error eliminando el libro " + req.params.id
                    });
            }
            else {
                if (resultado.affectedRows === 0) {
                    res.status(404).send({
                        ok: false,
                        mensaje: "El libro " + req.params.id + " no existe"
                    });
                }
                else {
                    res.status(200).send({
                        ok: true,
                        mensaje: "El libro " + req.params.id + " Ha sido eliminado correctamente"
                    });
                }
            }
        });
});

router.post('/', (req, res) => {
    let nuevoLibro = {
        COD: req.body.codigo,
        TITULO: req.body.titulo,
        PRECIO: req.body.precio,
        ISBN: req.body.isbn,
        IMAGEN: req.body.imagen,
        URL: req.body.url,
        COD_AUTOR: req.body.autor,
        ACTIVO: req.body.activo
    };

    fileName = utilImages.guardaImagenBase64(nuevoLibro.IMAGEN, nuevoLibro.COD);
    if (fileName === false) {
        res.status(500).send({
            ok: false,
            mensaje: "Error guardando el fichero de imagen"
        });
    }
    else {
        nuevoLibro.IMAGEN = config.directories.images + '/' + fileName;

        conexion.query("INSERT INTO libros SET ?",
            nuevoLibro, (error, resultado, campos) => {
                if (error)
                    res.status(400)
                        .send({
                            ok: false,
                            mensaje: "Error insertando el nuevo libro"
                        });
                else
                    res.status(200)
                        .send({
                            ok: true,
                            mensaje: resultado
                        });
            });
    }
});

router.put('/:id', (req, res) => {
    let libroEditado = {
        COD: req.body.codigo,
        TITULO: req.body.titulo,
        PRECIO: req.body.precio,
        ISBN: req.body.isbn,
        IMAGEN: req.body.imagen,
        URL: req.body.url,
        COD_AUTOR: req.body.autor,
        ACTIVO: req.body.activo
    };

    if (libroEditado.IMAGEN.length > 0) {
        fileName = utilImages.guardaImagenBase64(libroEditado.IMAGEN, libroEditado.COD);
        if (fileName === false) {
            res.status(500).send({
                ok: false,
                mensaje: "Error guardando el fichero de imagen"
            });
        }
    
        libroEditado.IMAGEN = config.directories.images + '/' + fileName;
    }

    conexion.query("UPDATE libros SET ? WHERE COD = ?",
        [ libroEditado, libroEditado.COD ], (error, resultado, campos) => {
            if (error)
                res.status(400)
                    .send({
                        ok: false,
                        mensaje: "Error editando el nuevo libro. " + error
                    });
            else
                res.status(200)
                    .send({
                        ok: true,
                        mensaje: resultado
                    });
        });
});

module.exports = router;