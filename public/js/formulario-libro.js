'use strict';

import { Libreria } from './libreria.js';

export class FormularioLibro {
    static validaCampoFormulario(nombreCampo) {
        let codigo = document.getElementById(nombreCampo);
        if (codigo.value.length === 0)
            return false;
        else
            return true;
    }
    
    static validaFormulario() {
        return (FormularioLibro.validaCampoFormulario("codigo") === true
            && FormularioLibro.validaCampoFormulario("titulo") === true
            && FormularioLibro.validaCampoFormulario("precio") === true
            && FormularioLibro.validaCampoFormulario("isbn") === true
            && FormularioLibro.validaCampoFormulario("imagen") === true);
    }

    static limpiarFormularioNuevoLibro() {
        document.getElementById('activo').checked = false;
        document.getElementById('codigo').value = '';
        document.getElementById('titulo').value = '';
        document.getElementById('precio').value = '';
        document.getElementById('isbn').value = '';
        document.getElementById('url').value = '';
        document.getElementById('imagen').value = '';
    }

    static cierraFormulario() {
        $("#formulario-libro").modal('hide');
        toastr.success('Operación realizada correctamente');
        Libreria.getLibros();
        FormularioLibro.limpiarFormularioNuevoLibro();
    }

    static adaptaFormularioLibro(libro) {
        let titulo = document.getElementById("modal-titulo-formulario");
        titulo.innerHTML = "Editando el libro <span>" + libro.COD + "</span>";
        titulo.className = "modal-title operacion-editar";
    
        document.getElementById('activo').checked = libro.ACTIVO;
        document.getElementById('codigo').value = libro.COD;
        document.getElementById('titulo').value = libro.TITULO;
        document.getElementById('precio').value = libro.PRECIO;
        document.getElementById('isbn').value = libro.ISBN;
        document.getElementById('url').value = libro.URL;
        document.getElementById('imagen').value = null;   
        
        Libreria.rellenaAutores(libro.COD_AUTOR);
    }

    static getDatosFormulario() {
        return  {
            codigo: document.getElementById("codigo").value,
            titulo: document.getElementById("titulo").value,
            precio: document.getElementById("precio").value,
            isbn: document.getElementById("isbn").value,
            url: document.getElementById("url").value,
            autor: document.getElementById("autor").value,
            activo: document.getElementById("activo").checked
        }
    }

    static estamosCreandoLibro() {
        let titulo = document.getElementById('modal-titulo-formulario');
        return titulo.className.includes("operacion-nuevo");
    }
}