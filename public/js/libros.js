function muestraLibro(libro) {
    document.getElementById('modal-titulo').innerHTML = libro.TITULO;

    document.getElementById('modal-cuerpo').innerHTML = `
        <img src="` + libro.IMAGEN + `" class="img-fluid" alt="Segway">
    `;
}

function getLibro(id) {
    return new Promise((resolve, reject) => {
        fetch('/libros/'+id)
            .then(response => response.json() )
            .then(respuesta => {
                if (respuesta.ok === true)
                    resolve(respuesta.data);
                else
                    reject();
            });
        })
}

function eliminarLibro(id) {
    fetch('/libros/' + id, { method: 'DELETE' })
        .then(function (response) {
            return response.json();
        }).then(function (respuesta) {
            if (respuesta.ok === true) {
                toastr.success('Se ha eliminado el libro con id ' + id, 'Operación realizada correctamente!!')
                getLibros();
            }
            else {
                alert("No se ha podido eliminar el libro");
            }
        });
}

function adaptaFormularioLibro(libro) {
    let titulo = document.getElementById("modal-titulo-formulario");
    titulo.innerHTML = "Editando el libro <span>" + libro.COD + "</span>";
    titulo.className = "modal-title operacion-editar";

    document.getElementById('activo').checked = libro.ACTIVO;
    document.getElementById('codigo').value = libro.COD;
    document.getElementById('titulo').value = libro.TITULO;
    document.getElementById('precio').value = libro.PRECIO;
    document.getElementById('isbn').value = libro.ISBN;
    document.getElementById('url').value = libro.URL;
    document.getElementById('imagen').value = null;   
    
    rellenaAutores(libro.COD_AUTOR);
}

function editaLibro(id) {
    getLibro(id)
        .then(libro => adaptaFormularioLibro(libro))
        .catch(error => Swal.fire(
                'Error!',
                'Los datos del formulario no son válidos! ' + error,
                'error'
            )
        );
}

function actualizaLibros(libros) {
    let rowAlbum = document.getElementById('rowAlbum');

    rowAlbum.innerHTML = "";

    libros.filter(libro => libro.ACTIVO).forEach(
        libro => {
            rowAlbum.innerHTML += `
            <div class="col-md-4 card-deck">
                <div class="card mb-4 shadow-sm">
                <img src="` + libro.IMAGEN + `" class="card-img-top" alt="Segway">
                <div class="card-body">
                    <p class="card-text">` + libro.TITULO + `</p>
                    <p class="card-text">` + libro.COD_AUTOR + `</p>
                    <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                        <button id="boton-detalles-` + libro.COD + `" type="button" class="boton-detalles btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#ver-libro">
                        <i class="fas fa-eye"></i>
                        </button>
                        <button id="boton-editar-` + libro.COD + `" class="boton-editar btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#formulario-libro"><i class="fas fa-edit"></i></button>
                        <button id="boton-eliminar-` + libro.COD + `" type="button" class="boton-eliminar btn btn-sm btn-outline-secondary"><i class="fas fa-trash"></i></button>
                    </div>
                    <small class="text-muted">` + libro.PRECIO.toLocaleString('es-ES', {
                style: 'currency',
                currency: 'EUR',
            }); + `</small>
                    </div>
                </div>
                </div>
            </div>            
            `;
        }
    );

    let botonesDetalles = Array.from(document.querySelectorAll('.boton-detalles'));

    botonesDetalles.forEach(boton => {
        boton.addEventListener('click', function () {
            let id = boton.id.substring("boton-detalles-".length);
            getLibro(id)
                .then(libro => muestraLibro(libro))
                .catch(error => Swal.fire(
                        'Error!',
                        'Los datos del formulario no son válidos!',
                        'error'
                    )
                );
        })
    });

    let botonesEditar = Array.from(document.querySelectorAll('.boton-editar'));

    botonesEditar.forEach(boton => {
        boton.addEventListener('click', function () {
            let id = boton.id.substring("boton-editar-".length);
            editaLibro(id);
        })
    });

    let botonesEliminar = Array.from(document.querySelectorAll('.boton-eliminar'));

    botonesEliminar.forEach(boton => {
        boton.addEventListener('click', function () {
            let id = boton.id.substring("boton-eliminar-".length);
            Swal.fire({
                title: '¿Seguro que quieres eliminar el libro con id ' + id + '?',
                text: "Una vez eliminado no se podrá recuperar",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, elimínalo!',
                cancelButtonText: 'No, me arrepiento!'
            }).then((result) => {
                if (result.value) {
                    eliminarLibro(id);
                }
            })
        })
    });
}


function validaCampoFormulario(nombreCampo) {
    let codigo = document.getElementById(nombreCampo);
    if (codigo.value.length === 0)
        return false;
    else
        return true;
}
function validaFormulario() {
    return (validaCampoFormulario("codigo") === true
        && validaCampoFormulario("titulo") === true
        && validaCampoFormulario("precio") === true
        && validaCampoFormulario("isbn") === true
        && validaCampoFormulario("imagen") === true);
}

function limpiarFormularioNuevoLibro() {
    document.getElementById('activo').checked = false;
    document.getElementById('codigo').value = '';
    document.getElementById('titulo').value = '';
    document.getElementById('precio').value = '';
    document.getElementById('isbn').value = '';
    document.getElementById('url').value = '';
    document.getElementById('imagen').value = '';
}

function getBase64(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
}

function estamosCreandoLibro() {
    let titulo = document.getElementById('modal-titulo-formulario');
    return titulo.className.includes("operacion-nuevo");
}

function actualizaLibroAjax(method, libro) {
    return new Promise((resolve, reject) => {
        let url = "http://localhost/libros";
        if (estamosCreandoLibro() === false)
            url += '/' + libro.COD;
        fetch(url, {
            method: method,
            body: JSON.stringify(libro),
            headers: { 'Content-Type': 'application/json' }
        })    
        .then(res => res.json())
        .then(res => resolve(res))
        .catch(error => reject(error));
    });
}

function actualizaLibro(method, libro)
{
    actualizaLibroAjax(method, libro)
    .then(respuesta => {
        $("#formulario-libro").modal('hide');
        toastr.success('Operación realizada correctamente');
        getLibros();
        limpiarFormularioNuevoLibro();
    })                
}

function guardarLibro() {
    let libro = {
        codigo: document.getElementById("codigo").value,
        titulo: document.getElementById("titulo").value,
        precio: document.getElementById("precio").value,
        isbn: document.getElementById("isbn").value,
        url: document.getElementById("url").value,
        autor: document.getElementById("autor").value,
        activo: document.getElementById("activo").checked
    }

    var file = document.getElementById('imagen').files[0];
    getBase64(file).then(
        data => {
            libro.imagen = data;
            if (estamosCreandoLibro() === true)
                actualizaLibro('POST', libro);
            else 
                actualizaLibro('PUT', libro);
        }
    );
}

function guardarLibroClick() {
    if (validaFormulario() === true)
        guardarLibro();
    else {
        Swal.fire(
            'Error!',
            'Los datos del formulario no son válidos!',
            'error'
        )
    }
}

function cargaAutoresEnSelect(autores, autorSeleccionado) {
    let selectAutor = document.getElementById('autor');
    Array.from(selectAutor.children).forEach(option => option.remove());
    
    autores.forEach(autor => {
        let nodeOption = document.createElement('option');
        nodeOption.value = autor.COD;
        if (autor.COD === autorSeleccionado)
            nodeOption.selected = true;
        let nodeText = document.createTextNode(autor.NOMBRE);
        nodeOption.appendChild(nodeText);
        selectAutor.appendChild(nodeOption);
    });
}

function obtenerAutores() {
    return new Promise((resolve, reject ) => {
        fetch('/autores')
        .then(response => response.json())
        .then(respuesta => {
            if (respuesta.ok === true) 
                resolve(respuesta.data);            
            else 
                reject(respuesta.mensaje);
        });
    });
}

function rellenaAutores(autorSeleccionado = '') {
    obtenerAutores()
        .then (autores => cargaAutoresEnSelect(autores, autorSeleccionado))
        .catch (error => Swal.fire(
                'Error!',
                'Los datos del formulario no son válidos!',
                'error'
        )
    );
}

function nuevoLibroClick() {
    let titulo = document.getElementById("modal-titulo-formulario");
    titulo.className = "modal-title operacion-nuevo";
    titulo.innerHTML = "Creando nuevo libro";

    rellenaAutores();
}