export class Autor {
    static cargaEnSelect(selectAutor, autor, autorSeleccionado) {
        let nodeOption = document.createElement('option');
        nodeOption.value = autor.COD;
        if (autor.COD === autorSeleccionado)
            nodeOption.selected = true;
        let nodeText = document.createTextNode(autor.NOMBRE);
        nodeOption.appendChild(nodeText);
        selectAutor.appendChild(nodeOption);
    }
}