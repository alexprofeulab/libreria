'use strict';

import { PeticionAjax } from "./peticionAjax.js";
import { Libreria } from "./libreria.js";
import { FormularioLibro } from './formulario-libro.js';
import { UtilImages } from "./util-images.js";

export class Libro {
    #id;

    constructor(id) {
        this.#id = id;
    }

    muestraLibro(libro) {
        document.getElementById('modal-titulo').innerHTML = libro.TITULO;
    
        document.getElementById('modal-cuerpo').innerHTML = `
            <img src="` + libro.IMAGEN + `" class="img-fluid" alt="Segway">
        `;
    }
    
    getLibro() {
        PeticionAjax.get('/libros/'+this.#id)
        .then(libro => this.muestraLibro(libro))
        .catch(error => Swal.fire(
                'Error!',
                'Error al obtener los datos del libro ' + this.#id + ': ' + error,
                'error'
            )
        );
    }
    
    editaLibro() {
        PeticionAjax.get('/libros/'+this.#id)
        .then(libro => FormularioLibro.adaptaFormularioLibro(libro))
        .catch(error => Swal.fire(
            'Error!',
            'Error editando el libro ' + this.#id + '. Error: ' + error,
            'error'
            )
        );
    }

    eliminaLibro() {
        PeticionAjax.delete('/libros/' + this.#id)
        .then(respuesta => {
            toastr.success('Se ha eliminado el libro con id ' + this.#id, 'Operación realizada correctamente!!')
            Libreria.getLibros();
        })
        .catch(error => Swal.fire(
            'Error!',
            'Error eliminando el libro ' + this.#id + '. Error: ' + error,
            'error'
            )
        );
    }

    static generaHTMLLibro(libro) {
        return `
        <div class="col-md-4 card-deck">
            <div class="card mb-4 shadow-sm">
            <img src="` + libro.IMAGEN + `" class="card-img-top" alt="Segway">
            <div class="card-body">
                <p class="card-text">` + libro.TITULO + `</p>
                <p class="card-text">` + libro.COD_AUTOR + `</p>
                <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                    <button id="boton-detalles-` + libro.COD + `" type="button" class="boton-detalles btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#ver-libro">
                    <i class="fas fa-eye"></i>
                    </button>
                    <button id="boton-editar-` + libro.COD + `" class="boton-editar btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#formulario-libro"><i class="fas fa-edit"></i></button>
                    <button id="boton-eliminar-` + libro.COD + `" type="button" class="boton-eliminar btn btn-sm btn-outline-secondary"><i class="fas fa-trash"></i></button>
                </div>
                <small class="text-muted">` + libro.PRECIO.toLocaleString('es-ES', {
            style: 'currency',
            currency: 'EUR',
        }); + `</small>
                </div>
            </div>
            </div>
        </div>            
        `;
    }

    static nuevoLibro(libro)
    {
        PeticionAjax.post('/libros', libro)
        .then(respuesta => FormularioLibro.cierraFormulario())
        .catch(error => Swal.fire(
            'Error!',
            'Error creando el libro. Error: ' + error,
            'error'
            )
        );
    }

    static actualizaLibro(libro)
    {
        PeticionAjax.put('/libros/' + libro.codigo, libro)
        .then(respuesta => FormularioLibro.cierraFormulario())               
        .catch(error => Swal.fire(
            'Error!',
            'Error editando el libro. Error: ' + error,
            'error'
            )
        );
    }
    
    static guardarLibro() {
        let libro = FormularioLibro.getDatosFormulario();
    
        var file = document.getElementById('imagen').files[0];
        UtilImages.getBase64(file).then(
            data => {
                libro.imagen = data;
                if (FormularioLibro.estamosCreandoLibro() === true)
                    Libro.nuevoLibro(libro);
                else 
                    Libro.actualizaLibro(libro);
            }
        );
    }    
}