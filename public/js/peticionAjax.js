
export class PeticionAjax {
    static peticion(url, method='GET', body=null, headers=null) {
        let settings = {
            method: method
        };
        if (body !== null)
            settings.body = body;
        if (headers !== null)
            settings.headers = headers;
        return new Promise((resolve, reject) => {
            fetch(url, settings)
            .then(response => response.json())
            .then(respuesta => {
                if (respuesta.ok === true) 
                    resolve(respuesta.data);            
                else 
                    reject(respuesta.mensaje);
            });
        });
    }
    static get(url) {
        return PeticionAjax.peticion(url);
    }        

    static delete(url) {
        return PeticionAjax.peticion(url, 'DELETE');
    }

    static post(url, obj) {
        return PeticionAjax.peticion(
            url, 
            'POST', 
            JSON.stringify(obj), 
            { 'Content-Type': 'application/json' }
        );
    }

    static put(url, obj) {
        return PeticionAjax.peticion(
            url, 
            'PUT', 
            JSON.stringify(obj), 
            { 'Content-Type': 'application/json' }
        );
    }
}
