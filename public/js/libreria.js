'use strict';

import {PeticionAjax} from './peticionAjax.js';
import { Autor } from './autor.js';
import { Libro } from './libro.js';
import { Botones } from './botones.js';
import {FormularioLibro } from './formulario-libro.js';

export class Libreria {
    static cargaAutoresEnSelect(autores, autorSeleccionado) {
        let selectAutor = document.getElementById('autor');
        Array.from(selectAutor.children).forEach(option => option.remove());
        
        autores.forEach(autor => {  
            Autor.cargaEnSelect(selectAutor, autor, autorSeleccionado);
        });
    }   

    static rellenaAutores(autorSeleccionado = '') {
        PeticionAjax.get("/autores")
            .then (autores => Libreria.cargaAutoresEnSelect(autores, autorSeleccionado))
            .catch (error => Swal.fire(
                    'Error!',
                    'No se han podido obtener los autores. Error: ' + error,
                    'error'
            )
        );
    }
    
    static actualizaLibros(libros) {
        let rowAlbum = document.getElementById('rowAlbum');
    
        rowAlbum.innerHTML = "";
    
        libros.filter(libro => libro.ACTIVO).forEach(
            libro => rowAlbum.innerHTML += Libro.generaHTMLLibro(libro)
        );
    
        Botones.inicia("boton-detalles");
        Botones.inicia("boton-editar");
        Botones.inicia("boton-eliminar");
    }
    
    static getLibros() {
        PeticionAjax.get('/libros')
        .then(libros => Libreria.actualizaLibros(libros))
        .catch(error => Swal.fire(
                'Error!',
                'No se han podido obtener los libros del servidor: ' + error,
                'error'
        ));
    }
    
    static guardarLibroClick() {
        if (FormularioLibro.validaFormulario() === true)
            Libro.guardarLibro();
        else {
            Swal.fire(
                'Error!',
                'Los datos del formulario no son válidos!',
                'error'
            )
        }
    }
    
     static nuevoLibroClick() {
        let titulo = document.getElementById("modal-titulo-formulario");
        titulo.className = "modal-title operacion-nuevo";
        titulo.innerHTML = "Creando nuevo libro";
    
        Libreria.rellenaAutores();
    }

    static iniciaEventos() {
        let botonCrearLibro = document.getElementById("boton-guardar-libro");
        botonCrearLibro.addEventListener('click', Libreria.guardarLibroClick);
    
        let botonNuevoLibro = document.getElementById("boton-nuevo-libro");
        botonNuevoLibro.addEventListener('click', Libreria.nuevoLibroClick);
    }

    static inicia() {
        Libreria.getLibros();
        Libreria.iniciaEventos();
    }
}