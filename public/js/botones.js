'use strict';

import { Libro } from './libro.js';

export class Botones {
    static eliminaLibroPreguntando(libro) {
        Swal.fire({
            title: '¿Seguro que quieres eliminar el libro con id ' + libro.COD + '?',
            text: "Una vez eliminado no se podrá recuperar",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, elimínalo!',
            cancelButtonText: 'No, me arrepiento!'
        }).then((result) => {
            if (result.value) {
                libro.eliminaLibro();
            }
        })

    } 

    static inicia(nombreClaseBotones) {
        let botones = Array.from(
            document.querySelectorAll('.'+nombreClaseBotones)
        );

        botones.forEach(boton => {
            boton.addEventListener('click', function () {
                let id = boton.id.substring((nombreClaseBotones+"-").length);
                let libro = new Libro(id);
                switch(nombreClaseBotones) {
                    case 'boton-detalles':
                        libro.getLibro();
                        break;
                    case 'boton-editar':
                        libro.editaLibro();
                        break;
                    case 'boton-eliminar':
                        Botones.eliminaLibroPreguntando(libro);
                        break;
                    default:
                        Swal.fire(
                            'Error!',
                            'La clase del libro no existe!',
                            'error'
                        )
                        break;
                }
            })
        });    
    }
}