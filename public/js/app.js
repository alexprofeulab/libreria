'use strict';

import {Libreria} from './libreria.js';

(function () {
    window.addEventListener('load', function () {
        Libreria.inicia();
    });
})();