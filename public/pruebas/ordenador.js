'use strict';

import {Product} from './product.js';

export class Ordenador extends Product {
    #ram;
    #procesador;

    constructor(ram, procesador) {
        super("Ordenador", 100);
        this.ram = ram;
        this.procesador = procesador;
    }
}