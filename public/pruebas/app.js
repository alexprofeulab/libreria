import {Product} from './product.js';
import {Ordenador} from './ordenador.js';

let p = new Product("Product", 50);
console.log(p.getTitle());
console.log(p.getDiscount(20));
console.log(p);

let o = new Ordenador('8GB', 'i7');
console.log(o.getTitle());
console.log(o.getDiscount(20));
console.log(o);

console.log(Product.getStoreName());
