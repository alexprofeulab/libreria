'use strict';

export class Product {
    #stock = 10;
    #nombre;
    #codigo;
    constructor (title, price) {
        this.price = price;
        this.nombre = title;
        this.codigo = 1;
    }

    getDiscount(discount) {
        this.totalDisc = this.price * discount / 100;
        return this.totalDisc;
    }

    getTitle() {
        return this.nombre + " - " + this.codigo;
    }

    static getStoreName() {
        return "Nombre de la tienda";
    }
}