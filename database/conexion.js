const mysql = require('mysql');
const config = require(__dirname + '/../config/config');

let conexion = mysql.createConnection(config.db);

conexion.connect((error) => {
    if (error)
        console.log("Error al conectar con la BD:", err);
    else
        console.log("Conexión satisfactoria");
});

module.exports = conexion;