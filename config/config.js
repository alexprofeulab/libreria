
const config = {
    db: {
        host: "localhost",
        user: "libreria_user",
        password: "libreria",
        database: "libreria"
    }, 
    directories: {
        images: 'images'
    }
};

module.exports = config;