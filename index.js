const express = require('express');
const bodyParser = require('body-parser');

const libros = require(__dirname + '/routes/libros');
const autores = require(__dirname + '/routes/autores');

let app = express();

app.use(bodyParser.json({limit: '50mb'}));
app.use('/libros', libros);
app.use('/autores', autores);

app.use('/', express.static(__dirname + '/public'));

app.listen(80);